﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    GameObject Characters;
    public float movespeed;
    public float jumpheight;
    //public float dashwait;
    //public float dashspeed;

    public bool isgrounded = false;

    Rigidbody2D rb;
    public Vector2 playerSpeed = new Vector2();
    private Vector3 lastFrame = new Vector3();

    // Ben Shackman [2018-10-03] <bshackman@protonmail.com>
    //Temp Variable to access character.cs
    public Character characterScript;

    // Adam Giunta [9-20-18] <amgiunta.2016@mymail.becker.edu>
    public Weapon weapon;
    GameObject hand;
    private float distancetoground;
    bool isright;
    private Vector2 flip;
    private Vector2 flipleft;
    public bool isWalking = false;
    int LayerMask = 1 << 8;



    private float _initialScale;

    // Use this for initialization
    void Start()
    {
        distancetoground = GetComponent<Collider2D>().bounds.extents.y;
        LayerMask = ~LayerMask;
        flip = new Vector2(-transform.localScale.x, transform.localScale.y);
        flipleft = new Vector2(transform.localScale.x, transform.localScale.y);
        transform.localScale = flip;
        isright = true;
        characterScript = GetComponentInParent<Character>();
        rb = GetComponent<Rigidbody2D>();
        if (!transform.Find("Hand")) { hand = Instantiate<GameObject>(new GameObject("Hand"), transform); }
        else
            hand = transform.Find("Hand").gameObject;
        weapon = hand.GetComponentInChildren<Weapon>();

        _initialScale = transform.localScale.x;
    }

    // Adam Giunta [9-20-18] <amgiunta.2016@mymail.becker.edu>
    private void Update()
    {
        if (Input.GetButtonDown("Cancel")) {
            MenuMaster.menuMaster.ToggleMenu();
            if (MenuMaster.menuMaster.isActiveAndEnabled)
            {
                MenuMaster.menuMaster.OpenMenu("Pause Screen");
            }
            else {
                MenuMaster.menuMaster.CloseMenu("Pause Screen");
            }
        }
        if (Input.GetButtonDown("Fire1")) { weapon.Attack(); }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        playerSpeed = (transform.position - lastFrame) / Time.fixedDeltaTime;
        lastFrame = transform.position;
        RaycastHit2D floorcheck = Physics2D.Raycast(transform.position, Vector2.down, distancetoground + 0.1f, LayerMask);
        if (floorcheck.collider != null)
        {
            isgrounded = true;
        }
        if (Input.GetAxis("Horizontal") != 0f && playerSpeed.x != 0f && isgrounded) { isWalking = true; }
        else { isWalking = false; }

        Move();

        if (Input.GetButtonDown("Jump") && isgrounded == true)
        {
            rb.AddForce(transform.up * jumpheight);
            isgrounded = false;
        }

        // Ben Shackman [2018-10-30] <bshackman@protonmail.com> Clamped rotation so player can't get flipped. Was having an issue with it during testing.
        //float clampedRotationValue = Mathf.Clamp(rb.rotation, -30, 30);
        //rb.rotation = clampedRotationValue;
    }


    private void Move()
    {
        Vector3 translation = new Vector3(movespeed * characterScript.fearMultiplier * Input.GetAxisRaw("Horizontal"), 0f, 0f);

        transform.Translate(translation);
        if (Input.GetAxisRaw("Horizontal") != 0f)
        {
            transform.localScale = new Vector3(Input.GetAxisRaw("Horizontal") * Mathf.Abs(_initialScale), transform.localScale.y, transform.localScale.z);
            // Ben Shackman [2018-10-30] <bshackman@protonmail.com>
            //Made minor change to ensure that character would always fully flip.
        }
        if (Input.GetAxisRaw("Horizontal") != 0f) {
            transform.localScale = new Vector3((Input.GetAxisRaw("Horizontal") * Mathf.Abs(_initialScale)), transform.localScale.y, transform.localScale.z);
        }
    }

}