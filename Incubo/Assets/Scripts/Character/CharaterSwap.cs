﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharaterSwap : MonoBehaviour {
   
    public int swapped;
    public Vector3 position;

    [Tooltip("The characters that are playable in this level")]
    public List<GameObject> characters;

    // Use this for initialization
    void Awake () {
        position = transform.position;
        foreach (GameObject characterObject in characters) {
            GameObject new_obj = Instantiate<GameObject>(characterObject, transform.position, Quaternion.identity, transform);
            new_obj.name = characterObject.name;
            new_obj.SetActive(false);
        }

        ActivateChild(0);
    }
	
	// Update is called once per frame
	void Update () {
       if(Input.GetKeyDown("r"))
            swap();

        position = transform.GetChild(swapped).position;
    }

    public void ActivateChild(int index) {
        index = (int)Mathf.Repeat((index), transform.childCount);
        if (transform.GetChild(swapped).gameObject.activeSelf) { transform.GetChild(swapped).gameObject.SetActive(false); }
        swapped = index;
        transform.GetChild(swapped).gameObject.SetActive(true);
    }

    public int GetActiveChildIndex() {
        return (int) Mathf.Repeat(swapped, transform.childCount);
    }

    public void swap()
    {
        int newIndex = (int) Mathf.Repeat((swapped+1), transform.childCount);
        Debug.Log("the new child should be " + newIndex + " and swapped is now " + swapped);

        ActivateChild(newIndex);
    }
}
