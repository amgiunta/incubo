﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform player;
    public Vector3 offset;
    public CharaterSwap charater;

	// Use this for initialization
	void Start () {
        charater = FindObjectOfType<CharaterSwap>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        player = FindObjectOfType<PlayerController>().transform;
        transform.position = charater.position + offset;
    }
}
