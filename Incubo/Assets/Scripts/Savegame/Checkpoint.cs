﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[RequireComponent(typeof(Collider2D))]
public class Checkpoint : MonoBehaviour {

    [Tooltip("How much fear is regenerated after every second?")]
    public float resurectionRate;
    public bool active;

    [NonSerialized]
    List<Character> charactersInRange;

	// Use this for initialization
	void Start () {
        GetComponent<Collider2D>().isTrigger = true;
        charactersInRange = new List<Character>();
	}

    private IEnumerator Regenerate() {
        if (charactersInRange.Count > 0) {
            foreach (Character character in charactersInRange) {
                if (character.currentFear > 0)
                {
                    character.currentFear -= 1;
                    yield return new WaitForSeconds(1 / resurectionRate);
                    StartCoroutine(Regenerate());
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Character>()) {
            charactersInRange.Add(collision.GetComponent<Character>());
            StartCoroutine(Regenerate());
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && !active) {
            if (collision.GetComponent<PlayerController>().isgrounded)
            {
                active = true;
                GameManager.gameManager.Save();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && collision.GetComponent<Character>()) {
            active = false;
            charactersInRange.Remove(collision.GetComponent<Character>());
        }
    }
}
