﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using soundTool.soundManager;

public class CutsceneManager : MonoBehaviour {

    [System.Serializable]
    public struct CutsceneFrame
    {
        public Sprite backgroundImage;
        public AudioClip audioToPlay;
        public float clipVolume;

        public int timeToDisplay;
    }

    public Image baseImage;
    public Text baseText;

    public CutsceneFrame[] frames;
    bool cutsceneShown;
    bool cutsceneSkipped;

	// Use this for initialization
	void Start () {
        cutsceneShown = false;
        cutsceneSkipped = false;
        baseImage.gameObject.SetActive(false);
	}

    /// <summary>
    /// Shows cutscene stored in frames
    /// </summary>
    /// <returns>If cutscene was shown</returns>
    public bool StartCutscene()
    {
        if(!cutsceneShown)
        {
            StartCoroutine(ShowCutscene());
            cutsceneShown = true;
            return true;
        }
        else { return false; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.CompareTag("Player"))
        {
            Debug.Log(StartCutscene());
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            StopCoroutine(ShowCutscene());
            baseImage.gameObject.SetActive(false);
            SoundManager.StopAllUISounds();
            cutsceneSkipped = true;
            Time.timeScale = 1;
        }
    }

    IEnumerator ShowCutscene()
    {
        Time.timeScale = 0;
        baseImage.gameObject.SetActive(true);
        foreach(CutsceneFrame frame in frames)
        {
            if(!cutsceneSkipped)
            {
                baseImage.sprite = frame.backgroundImage;
                SoundManager.PlayUISound(frame.audioToPlay, frame.clipVolume);
            }

            yield return new WaitForSecondsRealtime(frame.timeToDisplay);
        }

        baseImage.gameObject.SetActive(false);
        Time.timeScale = 1;
        yield return new WaitForEndOfFrame();
    }
}
