﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LoadGameMenu : UIMenu {

    List<GameObject> activeButtons = new List<GameObject>();
    public Transform content;

	// Use this for initialization
	void Start () {
	}

    void OnEnable()
    {
        Debug.Log("The Load Save menu is initializing");
        UpdateList();
    }

    private void UpdateList() {
        ResetList();
        DirectoryInfo dir = new DirectoryInfo(GameManager.gameManager.GetSaveDirectory());
        Debug.Log(GameManager.gameManager.GetSaveDirectory());
        FileInfo[] files = dir.GetFiles();

        for (int index = 0; index < files.Length; index ++) {
            FileInfo file = files[index];

            GameObject newButton = Instantiate<GameObject>(CreateLoadButton(file), content);
            Button button = newButton.GetComponent<Button>();
            button.onClick.AddListener(delegate {
                GameManager.gameManager.LoadSave(file);
                Debug.Log("Loading the file " + file.Name);
            });
            activeButtons.Add(newButton);
        }
    }

    private GameObject CreateLoadButton(FileInfo file) {
        GameObject buttonObj = Resources.Load<GameObject>("Prefabs/UI/Load Save Button");
        Text buttonText = buttonObj.GetComponentInChildren<Text>();

        buttonText.text = file.Name;

        return buttonObj;
    }

    private void ResetList() {
        while (activeButtons.Count > 0) {
            GameObject button = activeButtons[0];
            activeButtons.Remove(button);
            Destroy(button);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
