﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class QuitButton : CustomButton {

    // Use this for initialization
    protected override void Start()
    {
        clickEvent = _QuitAction;
        base.Start();
    }

    public void _QuitAction() {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            Application.Quit();
        }
        else
        {
            GameManager.gameManager.LoadLevel("mainMenu");
        }
        return;
    }
}
