﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBGun : ProjectileWeapon {

    private float direction;

    private void FixedUpdate()
    {
        if (Input.GetAxisRaw("Horizontal") != 0) {
            direction = Input.GetAxisRaw("Horizontal");
        }

        transform.forward = new Vector3(direction, 0, 0);
    }

    public override void Attack()
    {

        base.Attack();
    }
}
